module.exports = async ({request, response, store}) => {
    if (200 !== response.getStatusCode()) {
        return;
    }

    const url = new URL(request.getUrl());

    if ('/graphql/auth' !== url.pathname) {
        return;
    }

    const jsonResponse = JSON.parse(response.getBody().toString());
    const token = jsonResponse?.data?.login?.access_token;

    if (! token) {
        return;
    }

    const storeKey = 'jwt_token';
    await store.setItem(storeKey, token);
}
