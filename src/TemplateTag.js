module.exports = {
    name: "jwt_token",
    displayName: "JWT-TOKEN",
    description: "JWT Token from GraphQL response",
    run: async ({store}) => {
        const storeKey = 'jwt_token';
        const token = await store.getItem(storeKey);

        return `Bearer ${token}`;
    }
}
